const { stylelint } = require('@coko/lint')

stylelint.ignoreFiles = ['**/FrontMatterGroup.js']

module.exports = stylelint
